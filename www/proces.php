<?php

require_once('config/config.inc.php');
require_once('lib/vendor/swiftmailer/swiftmailer/lib/swift_required.php');

$komunikat = array();

function sendMail($email, $nowehaslo, $displayName)
{

// Create the Transport
$transport = Swift_SmtpTransport::newInstance('moskit.uwm.edu.pl', 25)
  ->setUsername('powiadomienia-rci@uwm.edu.pl')
  ->setPassword('AORCI!1305')
  ;

/*
You could alternatively use a different transport such as Sendmail or Mail:

// Sendmail
$transport = Swift_SendmailTransport::newInstance('/usr/sbin/sendmail -bs');

// Mail
$transport = Swift_MailTransport::newInstance();
*/

// Create the Mailer using your created Transport
$mailer = Swift_Mailer::newInstance($transport);

// Create a message
$message = Swift_Message::newInstance('Twoje hasło w domenie UCZELNIA zastało pomyślnie zresetowane')
  ->setFrom(array('powiadomienia-rci@uwm.edu.pl' => 'Regionalne Centrum Informatyczne UWM'))
  ->setTo(array($email => $displayName))
  ->setSender('powiadomienia-rci@uwm.edu.pl')
  ->setReplyTo('powiadomienia-rci@uwm.edu.pl')
  ->setBody("Pomyślnie zresetowano hasło.\nTwoje nowe hasło: ".$nowehaslo."\nPoniżej znajduje się link do strony z instukcjami użytkownika portalu pracownik.uwm.edu.pl\nhttps://pracownik.uwm.edu.pl/pomoc")
  ;

// Send the message
#$result = $mailer->send($message);
if($mailer->send($message))
    {
    $komunikat['send_mail_komunikat'] = "Pomyślnie wysłano maila z nowym tymczasowym hasłem na adres: $email.";
    $komunikat['send_mail_status'] = '1';
    $komunikat['if_error'] = '0';
    
    }
else
    {
    $komunikat['send_mail_komunikat'] = "Nie udało się wysłać maila z tymczasowym hasłem. Proszę skontaktować się z administratorem. admin-rci@uwm.edu.pl";
    $komunikat['send_mail_status'] = '0';
    $komunikat['if_error'] = '1';
    }
return $komunikat;
}


function generateStrongPassword($length = 12, $add_dashes = false, $available_sets = 'luds')
{
	$sets = array();
	if(strpos($available_sets, 'l') !== false)
		$sets[] = 'abcdefghjkmnpqrstuvwxyz';
	if(strpos($available_sets, 'u') !== false)
		$sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
	if(strpos($available_sets, 'd') !== false)
		$sets[] = '23456789';
	if(strpos($available_sets, 's') !== false)
		$sets[] = '!@#$%&*?';
	$all = '';
	$password = '';
	foreach($sets as $set)
	{
		$password .= $set[array_rand(str_split($set))];
		$all .= $set;
	}
	$all = str_split($all);
	for($i = 0; $i < $length - count($sets); $i++)
		$password .= $all[array_rand($all)];
	$password = str_shuffle($password);
	if(!$add_dashes)
		return $password;
	$dash_len = floor(sqrt($length));
	$dash_str = '';
	while(strlen($password) > $dash_len)
	{
		$dash_str .= substr($password, 0, $dash_len) . '-';
		$password = substr($password, $dash_len);
	}
	$dash_str .= $password;
	return $dash_str;
}

function ldap_person($userPrincipalName, $email, $pesel, $username, $password, $ldaps_url)
{

$ldap_conn = ldap_connect( $ldaps_url, 636 ) or die('<b>Wystąpił błąd ERROR: ldap_connect w funkcji ldap_person(). Proszę skontaktować się z administratorem.</b>');

ldap_set_option($ldap_conn, LDAP_OPT_PROTOCOL_VERSION, 3);
ldap_set_option($ldap_conn, LDAP_OPT_REFERRALS, 0);
ldap_set_option($ldap_conn, LDAP_OPT_RESTART, 1);
ldap_set_option($ldap_conn, LDAP_OPT_SIZELIMIT, 1000);
#ldap_set_option(NULL, LDAP_OPT_DEBUG_LEVEL, 7);

$ldap_bind = ldap_bind($ldap_conn, $username, $password ) or die('<b>Wystąpił błąd ERROR: ldap_bind w funkcji ldap_person(). Proszę skontaktować się z administratorem.<b>' );

$base_dn = "OU=UWM,DC=uczelnia,DC=uwm,DC=edu,DC=pl";
$filter = "(&(objectCategory=person)(objectClass=user)(sAMAccountName=".$userPrincipalName."))";

$attributes = array("mail", "description", "department", "sAMAccountName", "userPrincipalName", "displayName", "extensionAttribute1");

$result = ldap_search($ldap_conn, $base_dn, $filter, $attributes) or die('<b>Wystąpił błąd ERROR: ldap_search w funkcji ldap_person(). Proszę skontaktować się z administratorem.</b>');

$entries = ldap_get_entries($ldap_conn, $result) or die('<b>Wystąpił błąd ERROR: ldap_get_entries w funkcji ldap_person(). Proszę skontaktować się z administratorem.</b>');

 
	if($entries["count"] > 0)
	    {
	    for ($y=0; $y<$entries["count"]; $y++)
		{
		$dn=$entries[$y]['dn'];
        	$samaccountname=$entries[$y]['samaccountname'][0];
		$description=$entries[$y]['description'][0];
		$mail=$entries[$y]['mail'][0];
		$userprincipalname=$entries[$y]['userprincipalname'][0];
		$displayname=$entries[$y]['displayname'][0];
		$department=$entries[$y]['department'][0];
		$pesel=$entries[$y]['extensionattribute1'][0];
		
		$arrAdPracownik = 
		array(
		    'dn' => $dn, 
		    'description' => $description, 
		    'sAMAccountName' => $samaccountname, 
		    'userPrincipalName' => $userprincipalname, 
		    'displayName' => $displayname, 
		    'mail' => $mail,
		    'pesel' => $pesel,
		    'department' => $department
		);
		}
		}
	else
		{
		$arrAdPracownik = '0';
		}

return $arrAdPracownik;

ldap_close($ldap_conn);
}

function ldapSetPasswd($dn, $nowehaslo, $username, $password, $ldaps_url)
{
$ldap_conn = ldap_connect( $ldaps_url, 636 ) or die('<b>Wystąpił błąd ERROR: ldap_connect w funkcji ldapSetPasswd(). Proszę skontaktować się z administratorem.</b>');

ldap_set_option($ldap_conn, LDAP_OPT_PROTOCOL_VERSION, 3);
ldap_set_option($ldap_conn, LDAP_OPT_REFERRALS, 0);
ldap_set_option($ldap_conn, LDAP_OPT_RESTART, 1);
ldap_set_option($ldap_conn, LDAP_OPT_SIZELIMIT, 1000);
#ldap_set_option(NULL, LDAP_OPT_DEBUG_LEVEL, 7);

$ldap_bind = ldap_bind($ldap_conn, $username, $password ) or die('<b>Wystąpił błąd ERROR: ldap_bind w funkcji ldapSetPasswd(). Proszę skontaktować się z administratorem.</b>');

$newPassword = "\"" . $nowehaslo . "\"";
    $len = strlen($newPassword);
    $newPassw = "";
    for($i=0;$i<$len;$i++) {
        $newPassw .= "{$newPassword{$i}}\000";
    }
    
$ldaprecord = array();
$ldaprecord['unicodepwd'] = $newPassw;
$ldaprecord['pwdLastSet'] = '-1';

if(ldap_mod_replace($ldap_conn, $dn, $ldaprecord))
    {
    $komunikat['ldap_set_passwd'] = "Pomyślnie zresetowano hasło.";
    $komunikat['ldap_set_passwd_status'] = '1';
    $komunikat['if_error'] = '0';
    }
else
    {
    $komunikat['ldap_set_passwd'] = 'Wystąpił nieoczekiwany błąd. Nie udało się zresetować hasła. Prosimy o kontakt z serwisem UWM.';
    $komunikat['ldap_set_passwd_status'] = '0';
    $komunikat['if_error'] = '1';
    }
return $komunikat;
}

$url = 'https://www.google.com/recaptcha/api/siteverify';
$privatekey = "6LfXOw8TAAAAAGH1lAuM7yq0f7DZj3bbuBTNsSSg";
$response = file_get_contents($url."?secret=".$privatekey."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
$data = json_decode($response);

if (isset($data->success) AND $data->success==true) {
            
			if(isset($_POST['userPrincipalName']))
				{
				$stat = array();
				$userPrincipalName = trim($_POST["userPrincipalName"]);
				$email = trim($_POST["email"]);
				$pesel = trim($_POST["pesel"]);
				
				$arrAdPracownik = ldap_person($userPrincipalName, $email, $pesel, $config["username"], $config["password"], $config["ldaps_url"]);
				
				if ($arrAdPracownik == '0')
				    {
				    $komunikat['ldap_no_person'] = "Nie znaleziono pracownika ($userPrincipalName) w domenie UCZELNIA.";
				    $komunikat['if_error'] = '1';
				    $stat['checkMail'] = '0';
				    $stat['checkPesel'] = '0';
				    }
				else
				{    
				if ($arrAdPracownik['mail'] != $email)
				    {
				    if ($arrAdPracownik['mail'] == $arrAdPracownik['sAMAccountName']."@uwm.edu.pl")
					{
					$komunikat['ldap_wrong_email_check'] = 'Podany adres email jest inny niż podany w systemie kadrowym. Aby zresetować hasło proszę o aktualzacje adresu email w dziale kadr. id: '.$arrAdPracownik['sAMAccountName'];
					$komunikat['if_error'] = '1';
					}
				    else if ($arrAdPracownik['mail'] == '')
					{
					$komunikat['ldap_wrong_email_check'] = 'Brak adresu email w systemie kadrowym. Aby zresetować hasło proszę o aktualzacje adresu email w dziale kadr. id: '.$arrAdPracownik['sAMAccountName'];
					$komunikat['if_error'] = '1';
					}
				    else
					{
					$komunikat['ldap_wrong_email'] = 'Podany adres email jest inny niż podany w systemie kadrowym. id: '.$arrAdPracownik['sAMAccountName'].". Prosimy o zweryfikowanie adresu email w dziale kadr!!!";
					$komunikat['if_error'] = '1';
					}
					
				    $stat['checkMail'] = '0';
				    }
				else 
				    {
				    $stat['checkMail'] = '1';
				    }
				
				if ($arrAdPracownik['pesel'] != $pesel)
				    {
				    $stat['checkPesel'] = '0';
				    $komunikat['ldap_wrong_pesel'] = 'Podany numer PESEL jest inny niż numer w systemie kadrowym. id: '.$arrAdPracownik['sAMAccountName'].". Prosimy o zweryfikowanie numeru PESEL w dziale kadr!!!";
				    $komunikat['if_error'] = '1';
				    }
				else
				    {
				    $stat['checkPesel'] = '1';
				    }
				}
				if($stat['checkMail'] == '1' && $stat['checkPesel'] == '1')
				{
				
				$nowehaslo = generateStrongPassword();
				
				$komunikat_ldap = ldapSetPasswd($arrAdPracownik['dn'], $nowehaslo, $config["username"], $config["password"], $config["ldaps_url"]);
				
				if($komunikat_ldap['if_error'] == '0')
					{
					$komunikat_mail = sendMail($email, $nowehaslo, $arrAdPracownik['displayName']);
					}
				else
					{
					$komunikat_mail['ldap_set_passwd_2'] = 'Nie udało się zresetować hasła. id: '.$arrAdPracownik['sAMAccountName'];
					}
				$komunikat = array_merge($komunikat_ldap,$komunikat_mail); 
				}
				
				}
			else
				{
				$komunikat['ldap_missing_data'] = 'Proszę uzupełnić brakujące dane. id: '.$arrAdPracownik['sAMAccountName'];
				$komunikat['if_error'] = '1';
				}
			
} 
else 
    {
    $komunikat['ldap_missing_capcha'] = "Proszę zaznaczyć 'Nie jestem robotem'.";
    }

$komunikat['client_ip'] = $_SERVER['REMOTE_ADDR'];
$komunikat['client_user_agent'] = $_SERVER['HTTP_USER_AGENT'];

echo json_encode($komunikat);

$datetime = date('Y-m-d H:i:s');
$log = ''; 

foreach ($komunikat as $komunikatNazwa => $komunikatWartosc )
{
$log .= "[".$komunikatNazwa."] => ".$komunikatWartosc." ";
}

$logcontent = $datetime." [checkMail] => ".$stat['checkMail']." [checkPesel] => ".$stat['checkPesel']." ".$log."\n";

file_put_contents('/dane/www/logs/reset_pwd.log', $logcontent, FILE_APPEND);

?>
