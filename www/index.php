<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Portal Resetu Hasła</title>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src='lib/js/jquery-2.1.4.min.js'></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" media="screen" href="lib/css/styles.css" >

<script>
var gIfChecked = false;

function gCallback() {
gIfChecked = true;
var div = document.getElementById("captcha_cell_2");
document.getElementById('captcha_cell_3_span').style.display = "none";
div.style.boxShadow = "0 0 5px #5cd053";
div.style.border = "1px solid #28921f";
div.style.background = "#fff url(lib/css/images/valid.png) no-repeat 98% center";
}

$(function() {
    $( "#pomoc" ).dialog({
      autoOpen: false,
	  width: 400,
      show: {
        effect: "blind",
        duration: 1000
      },
      hide: {
        effect: "explode",
        duration: 1000
      },
	  position: {
        my: 'center',
        at: 'center'
      },
	  dialogClass:"DialogBox",
    });
$(document).ready(function () {	
      $( "#pomoc" ).dialog( "open" );
    });
$( "#opener" ).click(function() {
      $( "#pomoc" ).dialog( "open" );
    });
});
	
</script>
</head>
<body>

<div id="bg_pattern">

<div id="main">

<div id="response">
	<div id="response_1"></div>
</div>

<div id="logo"></div>

<div id="form">

<!--<form name="uwm" id="uwm" class="uwm" action="proces.php" method="POST">-->

<form name="uwm" id="uwm" class="uwm">

<ul>
<li>

<h2>Reset hasła</h2>
<span class="required_notification"> Wymagane informacje</span>
</li>

<li>
<label for="userPrincipalName">Nazwa użytkownika</label>
<input type="text" name="userPrincipalName" id="userPrincipalName" required placeholder="Proszę podać nazwę użytkownika" size="40" tabindex="1" pattern="[0-9]{1,}" />
<span class="form_hint">Np. 1341"</span>
</li>

<li>
<label for="email">Adres email</label>
<input type="text" name="email" id="email" required placeholder="Proszę podać służbowy adres email" size="40" tabindex="2" pattern="[^$]+@([^$]+.uwm.edu.pl|uwm.edu.pl|man.olsztyn.pl)"/>
<span class="form_hint">Poprawny format "...@uwm.edu.pl"</span>
</li>

<li>
<label for="pesel">Numer PESEL</label>
<input type="text" name="pesel" id="pesel" required placeholder="Proszę podać numer PESEL" pattern="[0-9]{11}" size="40" tabindex="3" />
<span class="form_hint">Poprawny format "np: 84072900764"</span>
</li>

<li>
<div id="captcha_row">
<div id="captcha_cell_1"><label>Potwierdź</label></div>
<div id="captcha_cell_2"><div id="captcha2" class="g-recaptcha" data-callback="gCallback" data-sitekey="6LfXOw8TAAAAALdOGuPeNEYFm_GzLawRLofl_VEh" style="transform: scale(0.85);"></div></div>
<div id="captcha_cell_3"><span id="captcha_cell_3_span">Proszę zaznaczyć "Nie jestem robotem"</span></div>
<div>
</li>

<li>
<button class="submit" type="submit">Resetuj hasło</button><button class="pomoc" type="button" id="opener">Pomoc</button>
</li>

</ul>

</form>

<div id="pomoc" title="Pomoc">
<b>Aplikacja resetu hasła dostępna jest tylko z sieci UWM.</b><br />
Aby procedura resetu hasla przebiegła prawidłowo, w systemie kadrowym muszą znajdować się następujące dane pracownika: <br /><br />

<b>1. Numer Pesel</b><br />
<b>2. Aktualny służbowy adres email w domenie uwm.edu.pl lub uczelnia.uwm.edu.pl lub matman.uwm.edu.pl.</b><br /><br />

W przypadku braku powyższych danych w systemie kadrowym, reset hasła nie będzie możliwy.<br />
Jeżeli dane podane w formularzu zgadzają się z danymi w systemie kadrowym, hasło zostanie zresetowane.<br />
Nowe, wygenerowane hasło zostanie wysłane na podany w formularzu ades email.<br />
Podczas pierwszego logowania w portalu pracownik.uwm.edu.pl, system poprosi o zmianę hasła.<br />
Poniżej znajduje się link do strony zawierającej szczegółowe instrukcje użytkownika.<br /><br />
<a href="https://pracownik.uwm.edu.pl/pomoc/" target="_blank">https://pracownik.uwm.edu.pl/pomoc/</a>
<br /><br />
W razie problemów prosimy o kontakt: admin-rci@uwm.edu.pl
</div>

</div>

<script>
$(document).ready(function(){

var uwmForm = $("#uwm");
uwmForm.on("submit", function(e) {

e.preventDefault();
		var div = document.getElementById("captcha_cell_2");
		
		if(gIfChecked == false)
		{
		document.getElementById('captcha_cell_3_span').style.display = "inline";
		div.style.boxShadow = "0 0 5px #d45252";
		div.style.border = "1px solid #b03535";
		div.style.background = "#fff url(lib/css/images/red_asterisk.png) no-repeat 98% center";
		}
		else
		{
		document.getElementById('captcha_cell_3_span').style.display = "none";
		div.style.boxShadow = "0 0 5px #5cd053";
		div.style.border = "1px solid #28921f";
		div.style.background = "#fff url(lib/css/images/valid.png) no-repeat 98% center";
		
		$('#modal-overlay').show();            
                $.ajax({
            	    type: 'POST',
            	    url: 'proces.php',
            	    //dataType: 'json',
		    //data: data,
		    data: $("#uwm").serialize(), 
            	    success: function(responseData) 
            	    { 
			$('#modal-overlay').hide();
			$('#response_1').html("");
			$('#response_1').css('padding', '20px');
			$('#response_1').css('font-size', '14px');
			$('#response_1').css('font-weight', 'bold');

			try
			    {
			    responseData = $.parseJSON(responseData);
			    $.each(responseData, function(key,value) {
				
			    if (key == 'if_error' || key == 'checkMail' || key == 'checkPesel' || key == 'ldap_set_passwd_status' || key == 'send_mail_status')
				{
				value = '';
				}
			    else
				{
				$('#response_1').append(value + "<br />");
				}
			    });
					
		    	    if (responseData.if_error == '0')
				{
				$('#response_1').append("Teraz można się zalogować na portalu pracownik: <a href='https://pracownik.uwm.edu.pl'>https://pracownik.uwm.edu.pl</a>");
				$('#response').css('background-color', 'green');
				$("#form :input").prop("disabled", true);
				}
			    else
				{
				$('#response').css('background-color', 'red');
				}

			    $('#response').show({
				effect: "blind",
				duration: 1000
				}); 
			    grecaptcha.reset();
            		    }
			catch(e)
			    {
			    $('#response').css('background-color', 'red');
			    $('#response_1').append(responseData + "<br />" + e);
			    $('#response').show({
				effect: "blind",
				duration: 1000
				}); 
						
			    grecaptcha.reset();
			    }
			    },
            	    
			error: function() {
			console.log('Wystąpił błąd w wywołaniu funkcji ajax!!!');
			$('#response_1').html("Wystąpił błąd w wywołaniu funkcji ajax!!!");
			}
            	    });
		}
		
        return false;
    });
});

</script>
<div id="footer">

Projekt i wykonanie: Alan Olesiński<br />
<?php
echo $_SERVER['REMOTE_ADDR'];
?>

</div>

</div>
</div>

<div id="modal-overlay">
    <div id="spinner"><img id="img-spinner" src="lib/css/loading.gif" alt="Łączenie..."/></div>
</div>

</body>
</html>
